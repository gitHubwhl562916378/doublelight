#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include <QTimer>
#include "doublelightobjrender.h"

class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void initializeGL() override;
    void resizeGL(int w,int h) override;
    void paintGL() override;

private:
    DoubleLightObjRender render_;
    QTimer tm_;
    QVector3D cameraLocation_,lightLocation_;
    QMatrix4x4 pMatrix_;
    qreal angleX_,angleY,anglZ_;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
