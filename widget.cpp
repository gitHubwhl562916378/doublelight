#include "widget.h"
#include "utils/objloader.h"

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent)
{
    connect(&tm_,SIGNAL(timeout()),this,SLOT(slotTimeout()));
//    tm_.start(30);
}

Widget::~Widget()
{

}

void Widget::initializeGL()
{
    render_.initsize("ch.obj");
    cameraLocation_.setX(0);
    cameraLocation_.setY(20);
    cameraLocation_.setZ(10);
    lightLocation_.setX(20);
    lightLocation_.setY(20);
    lightLocation_.setZ(20);
}

void Widget::resizeGL(int w, int h)
{
    pMatrix_.setToIdentity();
    pMatrix_.perspective(45,float(w)/h,0.01f,100.0f);
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0f,0.0f,0.0f,1.0f);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    QMatrix4x4 vMatrix;
    vMatrix.lookAt(cameraLocation_,QVector3D(0,0,0),QVector3D(0,0,-1));

    QMatrix4x4 mMatrix;
    mMatrix.translate(-1,0,0);
//    mMatrix.rotate(angleX_,1,0,0);
    mMatrix.rotate(angleY,0,1,0);
//    mMatrix.rotate(anglZ_,0,0,1);
    render_.render(f,pMatrix_,vMatrix,mMatrix,cameraLocation_,lightLocation_);
}

void Widget::slotTimeout()
{
    angleX_ += 5;
    angleY += 5;
    anglZ_ += 5;
    update();
}
