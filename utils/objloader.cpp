#include <QFile>
#include <QVector3D>
#include <QHash>
#include <QSet>
#include <QDebug>
#include "objloader.h"

uint qHash(const QVector3D &p, uint speed = 0)
{
    return qHash(QString("%1%2%3").arg(p.x()).arg(p.y()).arg(p.z()),speed);
}

bool ObjLoader::load(QString fileName, QVector<float> &vPoints,QVector<float> &nPoints)
{
    if(fileName.mid(fileName.lastIndexOf('.')) != ".obj"){
        qDebug() << "file is not a obj file";
        return false;
    }

    QFile objFile(fileName);
    if(!objFile.open(QIODevice::ReadOnly)){
        qDebug() << "open" << fileName << "failed";
        return false;
    }

    QVector<float> vertextPoints;
    QVector<int> faceIndexs;
    QHash<int,QSet<QVector3D>> normalHash;
    while (!objFile.atEnd()) {
        QByteArray lineData = objFile.readLine().simplified();
        if(lineData.isEmpty())continue;

        QList<QByteArray> strValues = lineData.split(' ');
        QString dataType = strValues.takeFirst();
        if(dataType == "v"){
            std::transform(strValues.begin(),strValues.end(),std::back_inserter(vertextPoints),[](QByteArray &str){
                return str.toFloat();
            });
        }else if(dataType == "f"){
            if(strValues.count() != 3){
                continue;
            }

            QVector<int> indexs;
            indexs << strValues.first().toInt() << strValues.at(1).toInt() << strValues.last().toInt();
            faceIndexs << indexs;
            QVector3D point1(vertextPoints.at((indexs.first() - 1) * 3),
                            vertextPoints.at((indexs.first() - 1) * 3 + 1),
                            vertextPoints.at((indexs.first() - 1) * 3 + 2));

            QVector3D point2(vertextPoints.at((indexs.at(1) - 1) * 3),
                            vertextPoints.at((indexs.at(1) - 1) * 3 + 1),
                            vertextPoints.at((indexs.at(1) - 1) * 3 + 2));

            QVector3D point3(vertextPoints.at((indexs.last() - 1) * 3),
                            vertextPoints.at((indexs.last() - 1) * 3 + 1),
                            vertextPoints.at((indexs.last() - 1) * 3 + 2));
            QVector3D side12 = point2 - point1;
            QVector3D side13 = point3 - point1;
            QVector3D vNormal = QVector3D::crossProduct(side12,side13);
            for(auto &tempIndex : indexs){
                normalHash[tempIndex].insert(vNormal);
            }
        }
    }
    objFile.close();

    for(int i = 0; i < faceIndexs.count(); i++){
        vPoints << vertextPoints.at((faceIndexs.at(i) - 1) * 3) << vertextPoints.at((faceIndexs.at(i) - 1) * 3 + 1) << vertextPoints.at((faceIndexs.at(i) - 1) * 3 + 2);
        QSet<QVector3D> normalsIndex = normalHash.value(faceIndexs.at(i));
        QVector3D averateNormal;
        foreach (const QVector3D &value, normalsIndex){
            averateNormal += value;
        }
        averateNormal.normalize();
        nPoints << averateNormal.x() << averateNormal.y() << averateNormal.z();
    }
}
